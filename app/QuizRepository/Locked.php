<?php

namespace App\QuizRepository;

class Locked
{

    public static function get($count)
    { // get the user's subcribed products

        $locked_quests = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

        for ($i = $count; $i < 16; $i++) {
            $locked_quests[$i] = "<span class='badge badge-pill badge-secondary'>Locked</span>";
        }

        return $locked_quests;

    }

    public static function formatted($count)
    { // get the user's subcribed products

        $locked_quests = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

        for ($i = $count; $i < 16; $i++) {
            $locked_quests[$i] = "true";
        }

        return $locked_quests;

    }

}
