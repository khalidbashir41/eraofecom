<?php

namespace App\QuizRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\SubscriptionRepository\UserSubscriptions;
use App\Quiz;

class QuizRecord {

    public static function save($productId, $questId, $percentageScore)
    {

        $userIid = Auth::id(); // authenticated user_id

        // first see whehter the data exist or not
        $obj = UserTrainingHistory::where('user_id', $userIid)
            ->where('product_id', $productId)
            ->where('quest_level', $questId)
            ->where('score', '>', 0)->get(); // this is a collection

        if ($obj->count() == 0) { // store the data only then if data hasnt been stored

            // $obj2 = UserTrainingHistory::where('user_id', $userIid)
            //                             ->where('product_id', $productId)
            //                             ->where('quest_level', $questId)
            //                             ->get(); // this is a collection

            $UserTrainingHistory = new UserTrainingHistory;
            $UserTrainingHistory->user_id = $userIid;
            $UserTrainingHistory->product_id = $productId;
            $UserTrainingHistory->quest_level = $questId;
            $UserTrainingHistory->score = $percentageScore;
            $UserTrainingHistory->video_watched = true; // makignt he video marked truye on quiz pass
            
            $UserTrainingHistory->save();
            
            return true; // successfully saved the data

        }else{
           

            return false;
        }
    }


}



