<?php

namespace App\ProductsRepository;

use App\Option;
use App\Quiz;
use App\QuizRepository\Locked;
use App\QuizRepository\Passed;
use App\Repository\Questions\Answers;
use App\Repository\Questions\RandomFive;
use App\UserTrainingHistory;
use Illuminate\Support\Facades\Auth;

class QuizzesJson
{

    public static function formatted($productId)
    {
        $user_id = Auth::id();

        $product_quizzes = Quiz::where('product_id', $productId)->get();

        $passed_quests = Passed::get($productId); // call this in order to update the $count property of Passesd Class

        $locked_quests = Locked::formatted(Passed::$count);

        foreach ($product_quizzes as $key => $value) {

            $product_quizzes[$key]['vid'] = $value['video_file'];

            $product_quizzes[$key]['phaseContent'] = $value['description'];

            $isQuizAlreadyPassed = UserTrainingHistory::where('quest_level', $value['level'])
                                                        ->where('score', '>=', '60')
                                                        ->where('user_id',$user_id)
                                                        ->where('product_id',$productId)->exists();

           

            $product_quizzes[$key]['isQuizAlreadyPassed'] = $isQuizAlreadyPassed;

            $product_quizzes[$key]['isLocked'] = $locked_quests[$key];

           

            $questions = RandomFive::get($value['id']); // get 5 random questions of the specific quiz
         
            $correctAnswers = Answers::get($questions);
          

            $product_quizzes[$key]['phaseQuestions'] = $questions;

            $quiz_level = $value['level'];

            $quiz_level_explode = explode('.', $quiz_level);

            // get the next section id
            switch ($quiz_level_explode[1]) {
                case 4:
                    $quiz_level += 0.7;

                    break;

                case 1:
                case 2:
                case 3:

                    $quiz_level += 0.1;

                    break;
                default:
                    # code...
                    break;

            }

            //dd($quiz_level);

            $nexQuizSectionId = Quiz::where('product_id', $productId)
                ->where('level', "$quiz_level")->pluck('id')->first();

            $product_quizzes[$key]['nextSection'] = $nexQuizSectionId;

            foreach ($questions as $secondKey => $secondValue) {

                $product_quizzes[$key]['phaseQuestions'][$secondKey]['quest'] = $secondValue['text'];

                // $product_quizzes[$key]['phaseQuestions'][$secondKey]['activeQuestion'] = true;

                //make ther last question to true
                if ($secondKey == 4) {
                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['lastQuestion'] = true;

                } else {
                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['lastQuestion'] = false;

                }

                if ($secondKey == 0) {

                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['activeQuestion'] = true;

                } else {

                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['activeQuestion'] = false;

                }

                $options = Option::where('question_id', $secondValue['id'])->get(); // here $secondValue['id'] is the question id

                $product_quizzes[$key]['phaseQuestions'][$secondKey]['options'] = $options;

                foreach ($product_quizzes[$key]['phaseQuestions'][$secondKey]['options'] as $thirdKey => $thirdValue) {

                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['options'][$thirdKey]['opt'] = $thirdValue['option'];

                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['options'][$thirdKey]['val'] = $thirdValue['value'];

                    $product_quizzes[$key]['phaseQuestions'][$secondKey]['options'][$thirdKey]['question_id'] = $thirdValue['question_id'];
                }

            }
            

        }

    

        return $product_quizzes;
    }

}
