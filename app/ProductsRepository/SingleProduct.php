<?php

namespace App\ProductsRepository;

use App\Product;

class SingleProduct
{

    public function __construct()
    {

    }

    public static function get($productId)
    {

        $product = Product::where('id', $productId)->get();

        return $product;

    }

}
