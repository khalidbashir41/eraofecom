<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Subscription;
use App\Plan;

class VerifyPlanProduct{

    function __construct() {
       
    }

    public function Access($product_id){
        
     

        $user_plan = Auth::user()->current_billing_plan; // user subscribed plan
        
       
        // now get the zone id for the subscribed plan
        $plan_zone = Plan::where('plan_id',$user_plan)->pluck('zone_id')->first();
       
        // now get the zone id for the subscribed plan
        $plan_includes = Plan::where('plan_id',$user_plan)->pluck('includes')->first();
       
        $product_type = Product::where('id',$product_id)
                        ->pluck('type')->first();
                    
        if($plan_includes == 'all'){ // user has subscribed to Master plan

            return true;
        
        }else if($plan_includes == 'designation' && $product_type == 'designation'){ // user has subscribed to Pro plan

            return true;
        
        }

        else{

            return false; // user has cancelled or not subscribed to any plan

        }

            
    }

    public static function formattedVerify($product_id){
        
     

        $user_plan = Auth::user()->current_billing_plan; // user subscribed plan
        
                    
        if($user_plan != ''){ // user has subscribed to Master plan

            return "true";
        
        }

        else{

            return "false"; // user has cancelled or not subscribed to any plan

        }

            
    }


}



