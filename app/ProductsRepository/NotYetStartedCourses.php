<?php

namespace App\ProductsRepository;

use App\Product;
use App\Tasks\Score;
use App\SubscriptionRepository\UserSubscriptions;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\ProductsRepository\NextQuizId;
use App\Tasks\IsUnderFreeTrial;

class NotYetStartedCourses
{

    public function __construct()
    {

    }

    public static function formatted()
    {
        $user_id = Auth::id();
        
        $products = UserSubscriptions::products();

        $isUnderFreeTrial = IsUnderFreeTrial::get();

        if($isUnderFreeTrial){

            $products = PublishedProducts::get();

        }

        
        
       // $products = [];

        foreach ($products as $key => $value) {
            
            // $exist = UserTrainingHistory::where('quest_level' ,'1.1' )
            //                              ->where('product_id', $value['id'])
            //                              ->exists();

            // $exist = UserTrainingHistory::where('quest_level' , '!=', 4.4)
            //                             ->where('product_id', $value['id'])
            //                             ->exists();

            $count = UserTrainingHistory::where('product_id', $value['id'])
                                          ->where('user_id',$user_id)
                                         ->count();

          
            if($count == 0){
                
              

                $products[$key]['courseName'] = $value['name']; // adding a new key with value same as of title for the sidebar navigation

                $products[$key]['url'] = "/scripts/$value[id]";
            
                $products[$key]['seal'] = $value['picture'];

                $products[$key]['phase'] = [];

                $products[$key]['phase']  = Score::result($value['id']);
            
                $score  = Score::result($value['id']);
            
                $products[$key]['modal'] = true;

                $firstQuizId = FirstQuizId::get($value['id']);

                $products[$key]['href'] = 'home#/training/' . $value['id'] . '/' . $firstQuizId;

            }

        }
        
        //dd($products);

        return $products;

    }

}
