<?php

namespace App\ProductsRepository;

use App\Product;
use App\Tasks\Score;
use App\SubscriptionRepository\UserSubscriptions;
use App\Tasks\IsUnderFreeTrial;

class TrainingScore
{

    public function __construct()
    {

    }

    public static function formatted()
    {
        $products = UserSubscriptions::products();

        $isUnderFreeTrial = IsUnderFreeTrial::get();

        if($isUnderFreeTrial){

            $products = PublishedProducts::get();

        }


        // $products = Product::where('publish',1)
        //                     ->where('zone_id',2)->get();

        foreach ($products as $key => $value) {

            $products[$key]['courseName'] = $value['name']; // adding a new key with value same as of title for the sidebar navigation

            $products[$key]['url'] = "/scripts/$value[id]";
           
            $products[$key]['seal'] = $value['picture'];

            $products[$key]['phase'] = [];

            $products[$key]['phase']  = Score::result($value['id']);
           
            $score  = Score::result($value['id']);
           
            $products[$key]['modal'] = true;
          

        }
        return $products;

    }

}
