<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Quiz;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\User;
use Illuminate\Support\Facades\DB;
use Laravel\Spark\Subscription;
use App\Proudct;
use App\Product;

class CustomerController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Customers');
            $content->description('List');
            
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
    

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
       
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {
            
            $grid->disableCreateButton();
            
            $grid->disableActions();

            $grid->id('ID')->sortable();

            $grid->uuid('licence#')->sortable();

            $grid->name('name');
            
            $grid->email('Email');

            $grid->country('Country');

            $grid->billing_city('Billing City');
            
            $grid->billing_country('Billing Country');

            $grid->billing_state('Billing State');
          
            $grid->created_at()->sortable();
           
            $grid->updated_at();

            $grid->filter(function ($filter) {

                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();

                $filter->like('name','Name');

                $filter->like('email','Email');

                $filter->like('country','country');

                $filter->like('billing_city','billing_city');

                $filter->like('billing_country','billing_country');

                $filter->like('billing_state','billing_state');

            });

            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Question::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
   
    }

    public function update(Request $request, $id){
        
    }
}
