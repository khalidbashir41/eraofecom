<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Product;
use App\ProductWorkoutCategory;
use App\WorkoutCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\ProductsRepository\AllProducts;

class ProductWorkoutCategoryController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {

        $this->products = $products;
        
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Product Workout Categories');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $ProductWorkoutCategory= ProductWorkoutCategory::where('id', $id)->get();
      
        $products= $this->products->allProducts(); 
      
        $workout_categories= WorkoutCategory::get();
        
        return view('vendor/admin/productworkoutcategories.edit',[
            'products' => $products,
            'workout_categories' => $workout_categories,
            'ProductWorkoutCategory' => $ProductWorkoutCategory[0]
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        
        $products= $this->products->allProducts(); 

        $workout_categories= WorkoutCategory::get();
        
        return view('vendor/admin/productworkoutcategories.create',[
            'products' => $products,
            'workout_categories' => $workout_categories
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ProductWorkoutCategory::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            // $grid->product_id('Product ID');
            $grid->product_id()->display(function($product_id) {
                return Product::find($product_id)->name;
            });
            // $grid->workout_category_id('Workout Category ID');
            $grid->workout_category_id()->display(function($workout_category_id) {
                return WorkoutCategory::find($workout_category_id)->title;
            });
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
       
            $ProductWorkoutCategory = new ProductWorkoutCategory();
            $ProductWorkoutCategory->product_id = $request->product_id;
            $ProductWorkoutCategory->workout_category_id = $request->workout_category_id;
            $ProductWorkoutCategory->save();
            return redirect('/admin/mamango=1/auth/product_categories');
       
    }

    public function update(Request $request, $id){
      
            $ProductWorkoutCategory = ProductWorkoutCategory::find($id);
            $ProductWorkoutCategory->product_id = $request->product_id;
            $ProductWorkoutCategory->workout_category_id = $request->workout_category_id;
            $ProductWorkoutCategory->save();

            return redirect('/admin/mamango=1/auth/product_categories');
        
    }

    public function destroy($id){
        ProductWorkoutCategory::destroy($id);
        return 'success';
    }
}
