<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Branding;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\ProductsRepository\AllProducts;

class BrandingController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {

        $this->products = $products;
        
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Branding');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $branding= Branding::where('id', $id)->get();
        $products= $this->products->allProducts();  
        
        return view('vendor/admin/branding.edit',[
            'branding' => $branding[0],
            'products' => $products
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        $products= $this->products->allProducts(); 
         
        return view('vendor/admin/branding.create',[
            'products' => $products
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Branding::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title('Title');
            $grid->type('Type');
            // $grid->product_id('Product');
            
            $grid->product_id()->display(function($product_id) {
                return Product::find($product_id)->name;
            });
            
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Branding::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
        if ($request->has('file')) {
        
            $branding = new Branding;
            $branding->title = $request->title;
            $branding->type = $request->type;
            $branding->product_id = $request->product_id;
            $branding->image= '';
            $branding->file = $request->file('file')->store('branding','s3');
            $branding->save();
            //dd($input);
            return redirect('/admin/mamango=1/auth/branding');
        }else{
            dd('File is required');
        }
    }

    public function update(Request $request, $id){
        if ($request->has('file')) {
            $directories = Storage::directories('');
            //dd($directories);
            $old_picture= Branding::where('id', $id)->pluck('file');
            $path = public_path()."/storage/$old_picture[0]";
           
            if($old_picture[0] != ""){
                //unlink($path); // first delete the file
            }
            
            // now update a product
            $branding = Branding::find($id);
            $branding->title = $request->title;
            $branding->type = $request->type;
            $branding->product_id = $request->product_id;
            $branding->image= '';
            $branding->file = $request->file('file')->store('branding','s3');
            $branding->save();

            return redirect('/admin/mamango=1/auth/branding');
        }else{
            $branding = Branding::find($id);
            $branding->title = $request->title;
            $branding->type = $request->type;
            $branding->product_id = $request->product_id;
            $branding->image= '';
            $branding->save();

            return redirect('/admin/mamango=1/auth/branding');
        }
        
    }
}
