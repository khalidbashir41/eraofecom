<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Product;
use App\WorkoutScript;
use App\WorkoutCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class WorkoutScriptController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Workout Scripts');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $WorkoutScript= WorkoutScript::where('id', $id)->get();
        $WorkoutCategories= WorkoutCategory::get(); 
        return view('vendor/admin/workoutscripts.edit',[
            'WorkoutScript' => $WorkoutScript[0],
            'WorkoutCategories' => $WorkoutCategories
            
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        $workout_categories= WorkoutCategory::get();
        return view('vendor/admin/workoutscripts.create',[
            'workout_categories' => $workout_categories
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(WorkoutScript::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            // $grid->workout_category_id('Workout Category');
            $grid->workout_category_id()->display(function($workout_category_id) {
                return WorkoutCategory::find($workout_category_id)->title;
            });
            $grid->title('title');
            $grid->description('description');
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
        if ($request->has('file')) {
        
            $WorkoutScript = new WorkoutScript();
            $WorkoutScript->title = $request->title;
            $WorkoutScript->description = $request->description;
            $WorkoutScript->workout_category_id = $request->workout_category_id;
            $WorkoutScript->file = $request->file('file')->store('workoutscripts','s3');
            $WorkoutScript->save();
            //dd($input);
            return redirect('/admin/mamango=1/auth/scripts');
        }else{
            dd('Audio file is required');
        }
    }

    public function update(Request $request, $id){
        if ($request->has('file')) {
            
            $old_picture= WorkoutScript::where('id', $id)->pluck('file');
           // $path = public_path()."/storage/$old_picture[0]";
            //unlink($path); // first delete the file
            
            // now update a product
            $WorkoutScript = WorkoutScript::find($id);
            $WorkoutScript->title = $request->title;
            $WorkoutScript->description = $request->description;
            $WorkoutScript->workout_category_id = $request->workout_category_id;
            $WorkoutScript->file = $request->file('file')->store('workoutscripts','s3');
            $WorkoutScript->save();

            return redirect('/admin/mamango=1/auth/scripts');
        }else{
            $WorkoutScript = WorkoutScript::find($id);
            $WorkoutScript->title = $request->title;
            $WorkoutScript->description = $request->description;
            $WorkoutScript->workout_category_id = $request->workout_category_id;
            $WorkoutScript->save();

            return redirect('/admin/mamango=1/auth/scripts');
        }
        
    }

    public function destroy($id){
        WorkoutScript::destroy($id);
        return 'success';
    }

}
