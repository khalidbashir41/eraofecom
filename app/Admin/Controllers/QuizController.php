<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Quiz;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\ProductsRepository\AllProducts;
use App\Level;

class QuizController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {

        $this->products = $products;
        
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Quizzes');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $quiz= Quiz::where('id', $id)->get();

        $products= $this->products->allProducts();
        
        $levels = Level::get();

        return view('vendor/admin/quizzes.edit',[
            'quiz' => $quiz[0],
            'products' => $products,
            'levels' => $levels
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

    
        $products= $this->products->allProducts();
        
        $levels = Level::get();

        return view('vendor/admin/quizzes.create',[
            'products' => $products,
            'levels' => $levels
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Quiz::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->level('Level');
            $grid->title('title');
            $grid->description('description');
            // $grid->product_id('Product');

            $grid->product_id()->display(function($product_id) {
                return Product::find($product_id)->name;
            });


            $grid->level_id()->display(function($level_id) {
                //return Level::find($level_id)->title;
            });
            

            $grid->filter(function ($filter) {
                
                $filter->equal('product_id','Product ID');
            });

            $grid->filter(function ($filter) {
                
                $filter->equal('level_id','level ID');
            });

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Quiz::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
       
        
            $Quiz = new Quiz;
            $Quiz->title = $request->title;
            $Quiz->intro = $request->intro;
            $Quiz->description = $request->description;
            $Quiz->product_id = $request->product_id;
            $Quiz->level= $request->level;
            $Quiz->level_id= $request->level_id;
            $Quiz->video_file = $request->video_file;
            $Quiz->free= $request->free;
            $Quiz->save();
            return redirect('/admin/mamango=1/auth/quizzes');
      
    }

    public function update(Request $request, $id){
       
            $Quiz = Quiz::find($id);
            $Quiz->title = $request->title;
            $Quiz->intro = $request->intro;
            $Quiz->description = $request->description;
            $Quiz->product_id = $request->product_id;
            $Quiz->level= $request->level;
            $Quiz->level_id= $request->level_id;
            $Quiz->video_file = $request->video_file;
            $Quiz->free= $request->free;
            $Quiz->save();
            return redirect('/admin/mamango=1/auth/quizzes');
      
        
    }

    public function destroy($id){
        return false; // so the quiz wont be able to delete from the admin
                     // if you need to make a quiz delete, just remove this destroy function from here.
    }
}
