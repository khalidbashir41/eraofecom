<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\PlanCategory;

class PlanCategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {

        return Admin::content(function (Content $content) {

            $content->header('Plan Categories');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $plan_category = PlanCategory::where('id', $id)->get();
        return view('vendor/admin/plancategories.edit', [
            'plan_category' => $plan_category[0],
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        
        return view('vendor/admin/plancategories.create', [

        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(PlanCategory::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title('Title');
            $grid->uuid('UUID');
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Tool::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request)
    {
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
        . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $random_number = str_shuffle($pin);

        $planCategory = new PlanCategory;
        $planCategory->title = $request->title;
        $planCategory->uuid = $random_number;
        $planCategory->save();
        return redirect('/admin/mamango=1/auth/plan_categories');

    }

    public function update(Request $request, $id)
    {

        $planCategory = PlanCategory::find($id);
        $planCategory->title = $request->title;
        $planCategory->save();
        return redirect('/admin/mamango=1/auth/plan_categories');

    }

    public function destroy($id)
    {
        // WorkoutCategory::destroy($id);

        // //also delete from the workout_scripts table -- also unlink audio
        // $deletedRows = WorkoutScript::where('workout_category_id', $id)->delete();

        // // also delete every row from the product_workout_categories table
        // $deletedRows = ProductWorkoutCategory::where('workout_category_id', $id)->delete();
        // return 'success';
    }
}
