<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Tool;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\ProductsRepository\AllProducts;


class ToolController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {

        $this->products = $products;
        
    }


    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Tools');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $tool= Tool::where('id', $id)->get();

        $products= $this->products->allProducts(); 
        
        return view('vendor/admin/tools.edit',[
            'tool' => $tool[0],
            'products' => $products
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        $products= $this->products->allProducts();

        return view('vendor/admin/tools.create',[
            'products' => $products
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Tool::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title('Title');
            // $grid->product_id('Product');

            $grid->product_id()->display(function($product_id) {
                return Product::find($product_id)->name;
            });
            
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Tool::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
       
        if ($request->has('file')) {
            
            $tool = new Tool;
            $tool->title = $request->title;
           
            $tool->free = $request->free;
            $tool->product_id = $request->product_id;
            $tool->file = $request->file('file')->store('tools','s3');
            $tool->image = $request->file('image')->store('tools', 's3');
            $tool->save();
            //dd($input);
            return redirect('/admin/mamango=1/auth/tools');
        }else{
            dd('File is required');
        }
    }

    public function update(Request $request, $id){
        
        $request->validate([
            'title' => 'required|max:500',
        ]);

        $tool = Tool::find($id);
        $tool->title = $request->title;
       
        $tool->free = $request->free;
        $tool->product_id = $request->product_id;

        if ($request->has('file')) {

            $tool->file = $request->file('file')->store('tools', 's3');

        } elseif ($request->has('image')) {

            $tool->image = $request->file('image')->store('tools', 's3');

        } else {

        }

        $tool->save();

            return redirect('/admin/mamango=1/auth/tools');
       
        
    }
}
