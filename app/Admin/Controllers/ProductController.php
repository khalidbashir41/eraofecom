<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\CourseCategory;
class ProductController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Products');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $product= Product::where('id', $id)->get(); 
        $course_categories =    CourseCategory::get();
        return view('vendor/admin/products.edit',[
            'product' => $product[0],
            'course_categories' => $course_categories
            
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        $course_categories =    CourseCategory::get();
        
        return view('vendor/admin/products.create',[

            'course_categories' => $course_categories

        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Product::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->name('name');
            $grid->price('price');
            $grid->plan_id('plan_id');
            $grid->course_category_id()->display(function ($course_category_id) {
                return CourseCategory::find($course_category_id)->name;
            });
            $grid->created_at();
            $grid->updated_at();
            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
        if ($request->has('picture')) {
            
           

            $product = new Product;
            $product->name = $request->name;
            $product->text = $request->text;
            $product->plan_id = $request->plan_id;
            $product->course_category_id = $request->course_category_id;
            $product->price = $request->price;
            $product->cost_price = $request->cost_price;
            $product->publish = $request->publish;
            $product->picture = $request->file('picture')->store('products','s3');
            $product->save();
            //dd($input);
            return redirect('/admin/mamango=1/auth/products');
        }else{
            //dd('Image is required');
        }
    }

    public function update(Request $request, $id){
        if ($request->has('picture')) {
            $directories = Storage::directories('');
            //dd($directories);
            // $old_picture= Product::where('id', $id)->pluck('picture');
            // $path = public_path()."/storage/$old_picture[0]";
            //unlink($path); // first delete the file
            
            // now update a product
            $product = Product::find($id);
            $product->name = $request->name;
            $product->text = $request->text;
            $product->plan_id = $request->plan_id;
            $product->course_category_id = $request->course_category_id;
            $product->price = $request->price;
            $product->cost_price = $request->cost_price;
            $product->publish = $request->publish;
            $product->picture = $request->file('picture')->store('products','s3');
            $product->save();

            return redirect('/admin/mamango=1/auth/products');
        }else{
            $product = Product::find($id);
            $product->name = $request->name;
            $product->text = $request->text;
            $product->plan_id = $request->plan_id;
            $product->course_category_id = $request->course_category_id;
            $product->price = $request->price;
            $product->cost_price = $request->cost_price;
            $product->publish = $request->publish;
            $product->save();

            return redirect('/admin/mamango=1/auth/products');
        }
        
    }

    public function destroy($id){
        return false; // so the product wont be able to delte from the admin
                     // if you need to make a product delte, just remove this destroy function from here.
    }
}
