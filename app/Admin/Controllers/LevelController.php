<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Plan;
use App\PlanCategory;
use App\Product;
use App\Zone;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Level;
use App\ProductsRepository\AllProducts;

class LevelController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {

        $this->products = $products;
        
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {

        return Admin::content(function (Content $content) {

            $content->header('Levels');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $level = Level::where('id', $id)->get()->first();
        
        $products= $this->products->allProducts();

        return view('vendor/admin/levels.edit', [
        
            'level' => $level,
            'products' => $products
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        $products= $this->products->allProducts(); 

        return view('vendor/admin/levels.create',[

            'products' => $products

        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Level::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title('title');
            $grid->product_id()->display(function($product_id) {
                return Product::find($product_id)->name;
            });
            $grid->total_videos('total_videos');
            $grid->total_duration('total_duration');
            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {

                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();

            });

            $grid->filter(function ($filter) {

                $filter->like('title', 'Title');

                $filter->like('total_videos', 'Total Videos');
            });

            $grid->filter(function ($filter) {
                
                $filter->equal('product_id','Product ID');
            });


        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request)
    {

        $level = new Level;
        $level->title = $request->title;
        $level->product_id = $request->product_id;
        $level->total_videos = $request->total_videos;
        $level->total_duration = $request->total_duration;
        $level->save();

        return redirect('/admin/mamango=1/auth/levels');

    }

    public function update(Request $request, $id)
    {

        $level = Level::find($id);
        $level->title = $request->title;
        $level->product_id = $request->product_id;
        $level->total_videos = $request->total_videos;
        $level->total_duration = $request->total_duration;
        $level->save();

        return redirect('/admin/mamango=1/auth/levels');

    }

    public function destroy($id)
    {
        return false; // so the product wont be able to delte from the admin
        // if you need to make a product delte, just remove this destroy function from here.
    }
}
