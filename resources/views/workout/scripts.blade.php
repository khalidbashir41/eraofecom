@extends('master')
@section('title', 'All workout scripts of ' . $category->title )
@section('page-title')
  @yield('title')
@endsection


@section('page-content')

@push('scripts')

<div id="app">
  <div class="row" >

  <div class="col-sm-6">
    <figure class="figure">
        <img src='{{asset("storage/$category->image")}}' class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
        <figcaption class="figure-caption">Total Scripts: {{$total_scripts}}</figcaption>
    </figure>
  </div>
    
  </div>

  <div class="row">
  @foreach ($workout_scripts as $script)   
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">{{$script->title}}</h5>
            <p class="card-text">{{$script->description}}</p>
         
            <audio preload="auto">
          
              <source src="{{Storage::disk('s3')->url($script->file)}}">
          
            </audio>
          
          </div>
      </div>
    </div>
  @endforeach
  </div>

</div>

<script src="{{asset('/audiojs/audio.min.js')}}"></script>
<script>
  audiojs.events.ready(function() {
    var as = audiojs.createAll();
  });
</script>

@endsection

