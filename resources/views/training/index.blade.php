@extends('master')
@section('title', 'Training')

@section('page-title')
  @yield('title')
@endsection

@section('page-content')
           <!-- =============== training-body =============== -->
            
            <div class="row">
            @foreach ($products as $product)
                <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" src='{{asset("storage/$product->picture")}}' alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">{{$product->name}}</h5>
                      <a href="training/{{$product->id}}" class="btn btn-primary">Start Now</a>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          <!-- =============== training-body ended=============== -->
@endsection
