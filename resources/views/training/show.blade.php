@extends('master')
@section('title', $productName )

@section('page-title')
  @yield('title')
@endsection

@section('page-content')
@push('scripts')


 @if (Session::has('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Congrats!</strong>  {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  @endif

  @if (Session::has('watched'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Congrats!</strong>  {{ session('watched') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  @endif

  @if (Session::has('error'))
       <div class="alert alert-danger alert-dismissible fade show" role="alert">
         <strong>Sorry!</strong>  {{ session('error') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
   @endif


    <link rel="stylesheet" href="{{ mix('/css/custom.css') }}" />



     

          @if(!is_null($progress))
            <div class="progress">
              <div class="progress-bar bg-success" role="progressbar" style="width: {{$progress}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$progress}}%</div>
            </div>
            <br />
          @endif

            <div class="row">
               <div class="col-sm-12 col-md-3 portal-dropdown">
                  <button class="btn btn-info dropdown-toggle custom-btn-stg" type="button" data-toggle="collapse" data-target="#level1" aria-expanded="false" aria-controls="level1">Level 1</button>
                  <div class="collapse show" id="level1">
                     <div class="card card-block">
                        <div class="list-group">
                          <div class="sub-btn-stg"><a @if($passed_quests[0] != '' || $passed_quests[0] == '' && $locked_quests[0] == '') href="/training/{{$product_id}}/{{$product_quests[0]}}"    @endif class="list-group-item list-group-item-success" >Phase: 1.1 {!! $passed_quests[0] !!} {!! $locked_quests[0]!!}</a></div>
                          <div class="sub-btn-stg">  <a @if($passed_quests[1] != '' || $passed_quests[1] == '' && $locked_quests[1] == '') href="/training/{{$product_id}}/{{$product_quests[1]}}"  @endif class="list-group-item list-group-item-info">Phase: 1.2 {!! $passed_quests[1] !!} {!! $locked_quests[1]!!}</a></div>
                          <div class="sub-btn-stg"> <a  @if($passed_quests[2] != '' || $passed_quests[2] == '' && $locked_quests[2] == '') href="/training/{{$product_id}}/{{$product_quests[2]}}"  @endif class="list-group-item list-group-item-warning">Phase: 1.3 {!! $passed_quests[2] !!} {!! $locked_quests[2]!!} </a></div>
                          <div class="sub-btn-stg">  <a @if($passed_quests[3] != '' || $passed_quests[3] == '' && $locked_quests[3] == '') href="/training/{{$product_id}}/{{$product_quests[3]}}"  @endif class="list-group-item list-group-item-danger">Phase: 1.4 {!! $passed_quests[3] !!} {!! $locked_quests[3]!!}</a></div>
                        </div>
                     </div>
                  </div>
                  <div>
                     <button class="btn btn-warning dropdown-toggle custom-btn-stg" type="button" data-toggle="collapse" data-target="#level2" aria-expanded="false" aria-controls="level2">Level 2</button>
                     <div class="collapse" id="level2">
                        <div class="card card-block">
                           <div class="list-group">
                              <div class="sub-btn-stg"> <a @if($passed_quests[4] != '' || $passed_quests[4] == '' && $locked_quests[4] == '')  href="/training/{{$product_id}}/{{$product_quests[4]}}"  @endif class="list-group-item list-group-item-success">Phase: 2.1 {!! $passed_quests[4] !!} {!! $locked_quests[4]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[5] != '' || $passed_quests[5] == '' && $locked_quests[5] == '') href="/training/{{$product_id}}/{{$product_quests[5]}}" @endif class="list-group-item list-group-item-info">Phase: 2.2 {!! $passed_quests[5] !!} {!! $locked_quests[5]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[6] != '' || $passed_quests[6] == '' && $locked_quests[6] == '') href="/training/{{$product_id}}/{{$product_quests[6]}}" @endif class="list-group-item list-group-item-warning">Phase: 2.3 {!! $passed_quests[6] !!} {!! $locked_quests[6]!!}</a></div>
                              <div class="sub-btn-stg"> <a @if($passed_quests[7] != '' || $passed_quests[7] == '' && $locked_quests[7] == '') href="/training/{{$product_id}}/{{$product_quests[7]}}" @endif class="list-group-item list-group-item-danger">Phase: 2.4 {!! $passed_quests[7] !!} {!! $locked_quests[7]!!}</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div>
                     <button class="btn btn-primary dropdown-toggle custom-btn-stg" type="button" data-toggle="collapse" data-target="#level3" aria-expanded="false" aria-controls="level3">Level 3</button>
                     <div class="collapse" id="level3">
                        <div class="card card-block">
                           <div class="list-group">
                              <div class="sub-btn-stg"> <a @if($passed_quests[8] != '' || $passed_quests[8] == '' && $locked_quests[8] == '') href="/training/{{$product_id}}/{{$product_quests[8]}}" @endif class="list-group-item list-group-item-success">Phase: 3.1 {!! $passed_quests[8] !!} {!! $locked_quests[8]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[9] != '' || $passed_quests[9] == '' && $locked_quests[9] == '') href="/training/{{$product_id}}/{{$product_quests[9]}}" @endif class="list-group-item list-group-item-info">Phase: 3.2 {!! $passed_quests[9] !!} {!! $locked_quests[9]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[10] != '' || $passed_quests[10] == '' && $locked_quests[10] == '') href="/training/{{$product_id}}/{{$product_quests[10]}}" @endif class="list-group-item list-group-item-warning">Phase: 3.3 {!! $passed_quests[10] !!} {!! $locked_quests[10]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[11] != '' || $passed_quests[11] == '' && $locked_quests[11] == '') href="/training/{{$product_id}}/{{$product_quests[11]}}" @endif class="list-group-item list-group-item-danger">Phase: 3.4 {!! $passed_quests[11] !!} {!! $locked_quests[11]!!}</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div>
                     <button class="btn btn-success dropdown-toggle custom-btn-stg" type="button" data-toggle="collapse" data-target="#level4" aria-expanded="false" aria-controls="level4">Level 4</button>
                     <div class="collapse" id="level4">
                        <div class="card card-block">
                           <div class="list-group">
                              <div class="sub-btn-stg"><a @if($passed_quests[12] != '' || $passed_quests[12] == '' && $locked_quests[12] == '') href="/training/{{$product_id}}/{{$product_quests[12]}}" @endif class="list-group-item list-group-item-success">Phase: 4.1 {!! $passed_quests[12] !!} {!! $locked_quests[12]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[13] != '' || $passed_quests[13] == '' && $locked_quests[13] == '') href="/training/{{$product_id}}/{{$product_quests[13]}}" @endif class="list-group-item list-group-item-info">Phase: 4.2 {!! $passed_quests[13] !!} {!! $locked_quests[13]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[14] != '' || $passed_quests[14] == '' && $locked_quests[14] == '') href="/training/{{$product_id}}/{{$product_quests[14]}}" @endif class="list-group-item list-group-item-warning">Phase: 4.3 {!! $passed_quests[14] !!} {!! $locked_quests[14]!!}</a></div>
                              <div class="sub-btn-stg"><a @if($passed_quests[15] != '' || $passed_quests[15] == '' && $locked_quests[15] == '') href="/training/{{$product_id}}/{{$product_quests[15]}}" @endif class="list-group-item list-group-item-danger">Phase: 4.4 {!! $passed_quests[15] !!} {!! $locked_quests[15]!!}</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>


                  <div class="col-md-1"></div>
                  <div class="col-sm-12 col-md-8">
                  <div>

                      @if($already_passed)
                          <h3> <span class="badge badge-info">Phase: {{$quest_level}}</span></h3>
                          <h4>Watch Video</h4>
                      @else
                      <h3> <span class="badge badge-info">Phase: {{$quest_level}}</span></h3>
                      <h4>Step 1: Watch the video</h4>
                      @endif


                      <form action="{{$questId}}/videoWatched" method="post" id="test">
                          @csrf
                        <iframe height="350px" width="90%" class="embed-responsive-item" src="{{$video_link}}" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        <a href="/training/{{$product_id}}/{{$questId}}/start-quiz"><button hidden class="btn btn-success" type="submit" >Un Un Un</button> </a>
                      </form>
                     
                      <p>{{$videoContent}}</p>
                      <hr />
                        @if(!$already_watched)
                          <h4>Step 2</h4>
                          <button type="button" class="btn btn-secondary btn-lg" disabled>Start Quiz</button>
                        @endif
                        
                        @if($already_watched)
                          @if(!$already_passed)
                              <h4>Step 2</h4>
                              <a href="/training/{{$product_id}}/{{$questId}}/start-quiz"><button class="btn btn-success" type="submit" >Start Quiz</button> </a>
                          @endif
                        @endif
                     

                  </div>
               </div><!-- col-sm-8 ended -->

            </div> <!-- row ended -->
          <!-- =============== training-body ended=============== -->

<script src="{{ asset('js/player.js') }}"></script>
<script src="{{ mix('/js/custom.js')}}"></script>
<script>
  
</script>

@endsection
