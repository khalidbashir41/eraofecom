@extends('master')
@section('title', 'Branding')

@section('page-title')
  @yield('title')
@endsection

@section('page-content')


          <!-- =============== Branding-body =============== -->
   @if (Session::has('error'))
       <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('error') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
   @endif

        <div class="row">  
          @foreach ($products as $product)
                <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" src='{{asset("storage/$product->picture")}}' alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">{{$product->name}}</h5>
                      <a href="../branding/{{$product->id}}" class="btn btn-primary">Start Now</a>
                    </div>
                  </div>
                </div>
          @endforeach

        </div>

         

        <!-- <div class="row">
            <div class="container">
              <p>You havent subscribed to any of the Licence or Designation yet. Click on the button to get started now.</p>
              <a href="settings#/subscription" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Subscribe Now</a>
            </div>
        </div> -->

       
          <!-- =============== training-body ended=============== -->
@endsection
