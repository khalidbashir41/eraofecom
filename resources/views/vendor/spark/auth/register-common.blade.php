<link rel="stylesheet" href="{{ mix('/css/checkout.css') }}">

<script  src="{{ mix('/js/tracking-codes/checkout.js') }}"></script>

<script  src="{{ mix('/js/tracking-codes/gtm.js') }}"></script>

<div class="row justify-content-center">
    <div class="col-lg-8">
        <!-- Coupon -->
        <div class="alert alert-success" v-if="coupon">
            <?php echo __('The coupon :value discount will be applied to your subscription!', ['value' => '{{ discount }}']); ?>
        </div>

        <!-- Invalid Coupon -->
        <div class="alert alert-danger" v-if="invalidCoupon">
            {{__('Whoops! This coupon code is invalid.')}}
        </div>

        <!-- Invitation -->
        <div class="alert alert-success" v-if="invitation">
            <?php echo __('teams.we_found_invitation_to_team', ['teamName' => '{{ invitation.team.name }}']); ?>
        </div>

        <!-- Invalid Invitation -->
        <div class="alert alert-danger" v-if="invalidInvitation">
            {{__('Whoops! This invitation code is invalid.')}}
        </div>
    </div>
</div>

<!-- Plan Selection -->
<div class="row justify-content-center custom-checkout-section" v-if="paidPlans.length > 0">
    <div class="col-lg-12">
        <div class="card card-default">
            <div class="card-header main-head-top">
                <!-- Get started with your 7 day free trial now!                 -->
            </div>
        </div>
    </div>
    <div class="col-lg-12">
       
        

         <div class="card card-default step1">
            <div class="card-header pb-1 pt-5">

                <h2 class="main-step-1">
                        <span v-if="paidPlans.length > 0">
                            <!-- {{__('Profile')}} -->
                            Your  @{{planName}} Plan Billing Cycle
                        </span>

                        <span v-else>
                            {{__('Register')}}
                        </span>
                </h2>
            
                <div class="card-body pb-0">
                    <p>Don't worry! Your card will not be charged for 7 days.</p>
                </div>

               
                <!-- Interval Selector Button Group -->
                <div class="float-right">
                    <div class="btn-group btn-group-sm" v-if="hasMonthlyAndYearlyPlans" style="padding-top: 2px;">
                        <!-- Monthly Plans -->
                        <button type="button" class="btn btn-light"
                                @click="showMonthlyPlans"
                                :class="{'active': showingMonthlyPlans}">

                            {{__('Monthly')}}
                        </button>

                        <!-- Yearly Plans -->
                        <button type="button" class="btn btn-light"
                                @click="showYearlyPlans"
                                :class="{'active': showingYearlyPlans}">

                            {{__('Yearly')}}
                        </button>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="table-responsive plan-portion-grel">
                <!-- Plan Error Message - In General Will Never Be Shown -->
                <div class="alert alert-danger m-4" v-if="registerForm.errors.has('plan')">
                    @{{ registerForm.errors.get('plan') }}
                </div>

                <!-- European VAT Notice -->
                @if (Spark::collectsEuropeanVat())
                    <p class="m-4">
                        {{__('All subscription plan prices are excluding applicable VAT.')}}
                    </p>
                @endif

                <table class="table table-responsive-sm table-valign-middle mb-0 ">
                    <thead></thead>
                    <tbody>
                        <tr v-for="plan in plansForActiveInterval">
                            <!-- Plan Name -->
                            <td>
                                <div class="d-flex align-items-center">
                                    <i v-if="!plan.picture" class="radio-select mr-2" @click="selectPlan(plan)"
                                    :class="{'radio-select-selected': isSelected(plan)}"></i>
                                    @{{ plan.reference_name }} 
                                    <!-- <span v-show="plan.picture"><img  style="width:33%;float:right" :src="'storage/' + plan.picture" /> </span> -->                                    
                                </div>                                
                            </td>
                            
                            <!-- Plan Features Button -->
                           

                            <!-- Plan Price -->
                            <!-- <td>
                                <span v-if="plan.price == 0" class="table-plan-text">
                                    {{__('Free')}}
                                </span>

                                <span v-else class="table-plan-text">
                                    <strong class="table-plan-price">@{{ plan.price | currency }}</strong>
                                    @{{ plan.type == 'user' && spark.chargesUsersPerSeat ? '/ '+ spark.seatName : '' }}
                                    @{{ plan.type == 'user' && spark.chargesUsersPerTeam ? '/ '+ __('teams.team') : '' }}
                                    @{{ plan.type == 'team' && spark.chargesTeamsPerSeat ? '/ '+ spark.teamSeatName : '' }}
                                    @{{ plan.type == 'team' && spark.chargesTeamsPerMember ? '/ '+ __('teams.member') : '' }}
                                    / @{{ __(plan.interval) | capitalize }}
                                </span>
                            </td> -->

                            <!-- Trial Days -->
                            <td class="table-plan-price table-plane-text text-right">
                                <span v-if="plan.trialDays">
                                    <?php echo __(':trialDays Day Trial', ['trialDays' => '{{ plan.trialDays }}']); ?>
                                </span>
                            </td>
                        </tr>                        
                        <tr class="course-include" v-for="plan in plansForActiveInterval">
                            <!-- <td><span class="plan-detail" v-if="plan.text">@{{plan.text}}</span></td> -->
                        </tr>
                    
                        
                        <tr v-for="plan in plansForActiveInterval">
                            <!-- <td><strong>Amount Due</strong></td> -->
                            <td>
                                <span v-if="plan.price == 0" class="table-plan-text">
                                    {{__('Free')}}
                                </span>

                                <!-- <span v-else class="table-plan-text">
                                    <strong class="table-plan-price">@{{ plan.price | currency }}</strong>
                                    @{{ plan.type == 'user' && spark.chargesUsersPerSeat ? '/ '+ spark.seatName : '' }}
                                    @{{ plan.type == 'user' && spark.chargesUsersPerTeam ? '/ '+ __('teams.team') : '' }}
                                    @{{ plan.type == 'team' && spark.chargesTeamsPerSeat ? '/ '+ spark.teamSeatName : '' }}
                                    @{{ plan.type == 'team' && spark.chargesTeamsPerMember ? '/ '+ __('teams.member') : '' }}
                                    / @{{ __(plan.interval) | capitalize }}
                                </span> -->
                            </td>
                        </tr>
                </tbody>
                </table>
                <span class="badge badge-secondary free-badge">TWO MONTHS FREE</span>

            </div>
        
        
        </div>
        
        <div class="card card-default step-2-offer">
            <div class="card-header">
                <h2 class="main-step-1">
                    <span v-if="paidPlans.length > 0">
                        <!-- {{__('Profile')}} -->
                        Create Your Login Info
                    </span>

                    <span v-else>
                        {{__('Register')}}
                    </span>
                </h2>
            </div>

            <div class="card-body">
                <!-- Generic Error Message -->
                <div class="alert alert-danger" v-if="registerForm.errors.has('form')">
                    @{{ registerForm.errors.get('form') }}
                </div>

                <!-- Invitation Code Error -->
                <div class="alert alert-danger" v-if="registerForm.errors.has('invitation')">
                    @{{ registerForm.errors.get('invitation') }}
                </div>

                <!-- Registration Form -->
                @include('spark::auth.register-common-form')
            </div>
          

        </div>

    </div>

<!-- Basic Profile -->
<!-- <div class="row justify-content-center">
    <div class="col-lg-8">
            
    </div>
</div> -->

