@extends('spark::layouts.app')

@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
@endsection

@section('content')
<spark-register-stripe inline-template>
    <div>
        <div class="spark-screen container">
            <!-- Common Register Form Contents -->
            @include('spark::auth.register-common')

            <!-- Billing Information -->
            <div class="col-lg-12" v-if="selectedPlan && selectedPlan.price > 0">
                
                <div class="card card-default step-3-offer">
                    <div class="card-header pb-0">
                        <!-- {{__('Billing Information')}} -->
                        <h2 class="main-step-1">Enter Your Billing Info</h2>
                        <div class="card-body">
                            
                            <p>Your card will not be charged for 7 days, then your monthly billing cycle will begin. If you cancel before 7 days, you will not be charged for your free trial.</p>

                            <img src="{{ asset('images/gurentee-seal.png') }}" alt="Guarentee Seal">

                        </div>
                    </div>

                    <div class="card-body">
                        <!-- Generic 500 Level Error Message / Stripe Threw Exception -->
                        <div class="alert alert-danger" v-if="registerForm.errors.has('form')">
                            {{__('We had trouble validating your card. It\'s possible your card provider is preventing us from charging the card. Please contact your card provider or customer support.')}}
                        </div>

                        <div class="step1">
                            <form role="form" class="form-inline">
                                <div class="row">
                                    <!-- Cardholder's Name -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="">{{__('Cardholder\'s Name')}}</label>                                    
                                            <input type="text" class="form-control type-text" name="name" v-model="cardForm.name">                                    
                                        </div>
                                    </div>

                                    <!-- Card Details -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name" class="">{{__('Card')}}</label>                                       
                                            <div id="card-element"></div>
                                            <span class="invalid-feedback" v-show="cardForm.errors.has('card')">
                                                @{{ cardForm.errors.get('card') }}
                                            </span>                                        
                                        </div>
                                    </div>

                                    <!-- Billing Address Fields -->
                                    @if (Spark::collectsBillingAddress())
                                        @include('spark::auth.register-address')
                                    @endif

                                    <!-- ZIP Code -->
                                    <div class="col-md-3">
                                        <div class="form-group" v-if=" ! spark.collectsBillingAddress">
                                            <label class="">{{__('ZIP / Postal Code')}}</label>                                            
                                            <input type="text" class="form-control type-text" name="zip" v-model="registerForm.zip" :class="{'is-invalid': registerForm.errors.has('zip')}">
                                            <span class="invalid-feedback" v-show="registerForm.errors.has('zip')">
                                                @{{ registerForm.errors.get('zip') }}
                                            </span>
                                        </div>
                                    </div>

                                    <!-- Coupon Code -->
                                    <div class="col-md-6 mt-4" v-if="query.coupon">                                        
                                        <div class="form-group">
                                            <label>{{__('Coupon Code')}}</label>
                                            <input type="text" class="form-control type-text" name="coupon" v-model="registerForm.coupon" :class="{'is-invalid': registerForm.errors.has('coupon')}">

                                            <span class="invalid-feedback" v-show="registerForm.errors.has('coupon')">
                                                @{{ registerForm.errors.get('coupon') }}
                                            </span>
                                        </div>
                                    </div>

                                    <!-- Terms And Conditions -->
                                    <div class="col-md-12 terms-cond">
                                        <div class="form-group row">                                        
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" v-model="registerForm.terms" >
                                                    {!! __('I Accept :linkOpen The Terms Of Service :linkClose', ['linkOpen' => '<a href="#" target="_blank">', 'linkClose' => '</a>']) !!}
                                                </label>
                                                <span class="invalid-feedback" v-show="registerForm.errors.has('terms')">
                                                    <strong>@{{ registerForm.errors.get('terms') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Tax / Price Information -->
                                    <div class="form-group row" v-if="spark.collectsEuropeanVat && countryCollectsVat && selectedPlan">
                                        <label class="col-md-4 col-form-label text-md-right">&nbsp;</label>

                                        <div class="col-md-6">
                                            <div class="alert alert-info" style="margin: 0;">
                                                <strong>{{__('Tax')}}:</strong> @{{ taxAmount(selectedPlan) | currency }}
                                                <br><br>
                                                <strong>{{__('Total Price Including Tax')}}:</strong>
                                                @{{ priceWithTax(selectedPlan) | currency }}
                                                @{{ selectedPlan.type == 'user' && spark.chargesUsersPerSeat ? '/ '+ spark.seatName : '' }}
                                                @{{ selectedPlan.type == 'user' && spark.chargesUsersPerTeam ? '/ '+ __('teams.team') : '' }}
                                                @{{ selectedPlan.type == 'team' && spark.chargesTeamsPerSeat ? '/ '+ spark.teamSeatName : '' }}
                                                @{{ selectedPlan.type == 'team' && spark.chargesTeamsPerMember ? '/ '+ __('teams.member') : '' }}
                                                / @{{ __(selectedPlan.interval) | capitalize }}
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Register Button -->
                                    <div class="col-md-12">
                                        <div class="form-group row mb-0">                                        
                                            <button type="submit" class="btn btn-primary submit-form-grel" @click.prevent="register" :disabled="registerForm.busy">
                                                <span v-if="registerForm.busy">
                                                    <i class="fa fa-btn fa-spinner fa-spin"></i> {{__('Registering')}}
                                                </span>

                                                <span v-else>
                                                    <i class="fa fa-btn fa-check-circle"></i> {{__('Start My Free Trial')}}
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    
                                    <div class="footer-offer-checkout">
                                        <p>By clicking 'Start My Free Trial' I agree to eraofecom.org <a href="#" target="_blank">Terms</a> and <a href="#" target="_blank">Privacy Policy</a></p>
                                        <img src="{{ asset('images/secure-checkout-icon.png') }}" alt="Secure Checkout">
                                  </div>

                                </div>
                            </form>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <!-- Plan Features Modal -->
        @include('spark::modals.plan-details')
    </div>
</spark-register-stripe>
@endsection


    
