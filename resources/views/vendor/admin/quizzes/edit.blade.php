<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Admin::title() }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/font-awesome/css/font-awesome.min.css") }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/dist/css/skins/" . config('admin.skin') .".min.css") }}">

    {!! Admin::css() !!}
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/laravel-admin/laravel-admin.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/nprogress/nprogress.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/sweetalert/dist/sweetalert.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/nestable/nestable.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/toastr/build/toastr.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/bootstrap3-editable/css/bootstrap-editable.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/google-fonts/fonts.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css") }}">

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/dist/js/app.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/jquery-pjax/jquery.pjax.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/nprogress/nprogress.js") }}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition {{config('admin.skin')}} {{join(' ', config('admin.layout'))}}">
<div class="wrapper">

    @include('admin::partials.header')

    @include('admin::partials.sidebar')

    <div class="content-wrapper" id="pjax-container">
        @yield('content')
       
        <div class="row">
            <div class="col-sm-12">
                <h2>Update a Quiz</h2>
            </div>

            <div class="col-sm-12">
                <form role="form" method="POST" action="update" enctype="multipart/form-data"> 
                @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control" value="{{$quiz->title}}" id="exampleInputEmail1" placeholder="Name" name="title">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Intro</label>
                            <input type="text" class="form-control" value="{{$quiz->intro}}" id="exampleInputEmail1" placeholder="Intro text" name="intro">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea class="form-control" rows="5"  placeholder="Enter ..." name="description">{{$quiz->description}}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputPassword1">Level</label>
                            <select class="form-control select2" style="width: 100%;" name="level">
                            
                                <option value="1.1" <?php if($quiz->level == "1.1"){?> selected="selected" <?php }?> >1.1</option>
                                <option value="1.2" <?php if($quiz->level ==  "1.2"){?> selected="selected" <?php }?> >1.2</option>
                                <option value="1.3" <?php if($quiz->level =="1.3"){?> selected="selected"  <?php }?> >1.3</option>
                                <option value="1.4" <?php if($quiz->level == "1.4"){?> selected="selected"  <?php }?>>1.4</option>
                                <option value="2.1" <?php if($quiz->level == "2.1"){?> selected="selected"  <?php }?>>2.1</option>
                                <option value="2.2" <?php if($quiz->level == "2.2"){?> selected="selected"  <?php }?>>2.2</option>
                                <option value="2.3" <?php if($quiz->level == "2.3"){?> selected="selected"  <?php }?> >2.3</option>
                                <option value="2.4" <?php if($quiz->level == "2.4"){?> selected="selected"  <?php }?> >2.4</option>
                                <option value="3.1" <?php if($quiz->level == "3.1") {?>selected="selected"  <?php }?>>3.1</option>
                                <option value="3.2" <?php if($quiz->level == "3.2") {?>selected="selected"  <?php }?>>3.2</option>
                                <option value="3.3" <?php if($quiz->level == "3.3"){?> selected="selected"  <?php }?>>3.3</option>
                                <option value="3.4" <?php if($quiz->level == "3.4"){?> selected="selected"  <?php }?> >3.4</option>
                                <option value="4.1" <?php if($quiz->level == "4.1"){?> selected="selected"  <?php }?> >4.1</option>
                                <option value="4.2" <?php if($quiz->level == "4.2"){?> selected="selected"  <?php }?>>4.2</option>
                                <option value="4.3" <?php if($quiz->level == "4.3"){?> selected="selected"  <?php }?>>4.3</option>
                                <option value="4.4" <?php if($quiz->level == "4.4"){?> selected="selected"  <?php }?>>4.4</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">level Category</label>
                            <select class="form-control select2" style="width: 100%;" name="level_id">
                                @foreach($levels as $level)
                                    <option value="{{$level->id}}" @if("{{$level->id}}" === "{{$quiz->level_id}}") selected="selected" @endif >{{$level->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        
                        <div class="form-group">
                            <label for="exampleInputPassword1">Product</label>
                            <select class="form-control select2" style="width: 100%;" name="product_id">
                                @foreach($products as $product)
                                <option value="{{$product->id}}" @if("{{$product->id}}" === "{{$quiz->product_id}}") selected="selected" @endif >{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Video Link</label>
                            <input type="text" class="form-control" value="{{$quiz->video_file}}" id="exampleInputEmail1" placeholder="Video Link" name="video_file">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Avilable on Free Trial</label>
                            <select class="form-control select2" style="width: 100%;" name="free">
                                <option value="1" @if($quiz->free == '1') selected="selected" @endif>Yes</option>
                                <option value="0" @if($quiz->free == '0') selected="selected" @endif >No</option>
                            </select>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
             </div>
        </div><!-- row ended-->

        {!! Admin::script() !!}
    </div>

    @include('admin::partials.footer')

</div>

<!-- ./wrapper -->

<script>
    function LA() {}
    LA.token = "{{ csrf_token() }}";
</script>

<!-- REQUIRED JS SCRIPTS -->
<script src="{{ asset ("/vendor/laravel-admin/nestable/jquery.nestable.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/toastr/build/toastr.min.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/bootstrap3-editable/js/bootstrap-editable.min.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/sweetalert/dist/sweetalert.min.js") }}"></script>
{!! Admin::js() !!}
<script src="{{ asset ("/vendor/laravel-admin/laravel-admin/laravel-admin.js") }}"></script>

</body>
</html>
