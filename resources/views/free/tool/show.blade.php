@extends('master')
@section('title', $licence . ' Tools')
@section('page-title')
  @yield('title')
@endsection


@section('page-content')

@push('scripts')

  <div class="row">

  @foreach ($tools as $tool)    

    <div class="col-sm-4">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">{{$tool->title}}</h5>
          <a href='{{Storage::disk("s3")->url($tool->file)}}' target="_blank" class="btn btn-primary">Download Now</a>
        </div>
      </div>
    </div>

  @endforeach

  @foreach ($paidTools as $tool)    

      <div class="col-sm-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{{$tool->title}}  <a href="/settings#/subscription"><span class="badge badge-warning">Unlock</span></a></h5>
          </div>
        </div>
      </div>

    @endforeach

  </div>

@endsection
