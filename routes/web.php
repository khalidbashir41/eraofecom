<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use  App\ProductsRepository\ProductCategories;
use App\ProductsRepository\Name as ProductName;
use App\Repository\Tools\AllJson;
use App\Tasks\Progress;
use App\ProductsRepository\LevelsJson;
use App\ProductsRepository\VerifyPlanProduct;
use App\ProductsRepository\QuizzesJson;
use App\Tasks\TrainingSectionScore;
use App\Tasks\BrandingAccess;
use App\Repository\Branding\AllJson  as AllBranding;
use App\ProductsRepository\CompletedCoursesScore;
use App\ProductsRepository\InProgressCourses;
use App\ProductsRepository\NotYetStartedCourses;
use App\ProductsRepository\TrainingScore;
use App\Tasks\IsUnderFreeTrial;
use App\QuizRepository\IsFree;
use App\Repository\WorkoutCategory\AllCategories;
use App\Repository\WorkoutCategory\ScriptsJson;
use App\Tasks\WorkoutCategoryName;

Auth::routes();

Route::get('/', 'WelcomeController@show');

Route::get('/thankyou', 'ThankyouController@index');

Route::get('/home', 'HomeController@index');
Route::resource('home', 'HomeController')->middleware('auth');


Route::get('training/{pid}/{questid}', 'TrainingController@show')->middleware('auth');
Route::get('training/{pid}/{questid}/start-quiz', 'TrainingController@startQuiz')->middleware('auth');
Route::put('training/{pid}/{questid}/evaluate', 'UserTrainingHistoryController@store')->middleware('auth');

Route::post('training/{pid}/{questid}/videoWatched', 'UserTrainingHistoryController@hasWatched')->middleware('auth');

Route::resource('training', 'TrainingController')->middleware('auth');

Route::get('presentation/{name}', 'ToolController@presentation')->middleware('auth');
Route::resource('tools', 'ToolController')->middleware('auth');

Route::get('branding/my_certificate/{id}', 'BrandingController@certificate')->middleware('auth');
Route::get('branding/download/{pid}/{id}', 'BrandingController@resource')->middleware('auth');
Route::resource('branding', 'BrandingController')->middleware('auth');

Route::get('workouts/{pid}/{categoryid}', 'WorkoutController@scripts')->middleware('auth');
Route::resource('workouts', 'WorkoutController')->middleware('auth');


Route::post('/subscribe', 'PlanController@post');
Route::post('/unsubscribe', 'PlanController@destroy');

Route::get('/randing/certificate', function(){
  return view('test');
});

//all category products
Route::get('/api/all/category/products', function(){
  return ProductCategories::formatted(); // boolean result
})->middleware('auth');

//get product name
Route::get('api/product/name/{id}', function($id){
  return ProductName::get($id); // all tools json
})->middleware('auth');

//get all tools of the product
Route::get('/api/all-tools/{product_id}', function($product_id){
  //return 's' ;
  return AllJson::get($product_id); // all tools json
})->middleware('auth');

Route::get('/api/product/progress/{product_id}', function($product_id){ // $product ID
  return  Progress::result($product_id);
})->middleware('auth');

//product quizzes for trianing section
Route::get('/api/product/levels/{id}', function($id){
  return  LevelsJson::formatted($id);
})->middleware('auth');

//verify access
Route::get('/api/verify/plan/product/{product_id}', function($product_id){ // $product ID
  return  VerifyPlanProduct::formattedVerify($product_id);
})->middleware('auth');


//get all the quizzes of the produyct with random 5 questions
Route::get('/api/product/all/quizzes/{id}', function($id){ // $product ID
  return  QuizzesJson::formatted($id);
})->middleware('auth');


Route::get('/api/training/section/submit/{product_id}/{quiz_id}/{question_ids}/{answers}', function($product_id,$quiz_id,$question_ids,$answers){ // $product ID
  return  TrainingSectionScore::evaluate($product_id,$quiz_id,$question_ids,$answers);
})->middleware('auth');

//verify has braning access
Route::get('/api/branding/has/access/{id}', function($id){
  return BrandingAccess::verifyInStringResponse($id); // boolean result
})->middleware('auth');


//get all the  brandings
Route::get('/api/all-brandings', function(){
  //return 's' ;
  return AllBranding::get(); // all tools json
})->middleware('auth');


//completed courses score
Route::get('/api/completed/course/training/score', function(){
  return CompletedCoursesScore::formatted(); // boolean result
})->middleware('auth');

//in progress courses score
Route::get('/api/in/progress/courses', function(){
  return InProgressCourses::formatted(); // boolean result
})->middleware('auth');

Route::get('/api/not/yet/started/courses', function(){
  return NotYetStartedCourses::formatted(); // boolean result
})->middleware('auth');

//trianing score
Route::get('/api/training/score', function(){
  return TrainingScore::formatted(); // boolean result
})->middleware('auth');

//is the user udner free trial/sign up
Route::get('/api/is/under/free/trial', function(){
  return IsUnderFreeTrial::get(); // boolean result
})->middleware('auth');

//check whether the user can access the quiz under free trial
Route::get('api/can/access/quiz/under/free/trial/{quiz_id}', function($quiz_id){
  return IsFree::get($quiz_id); // boolean result
})->middleware('auth');


//get all the workout categories
Route::get('api/workout/categories/all', function(){
     
  return AllCategories::formatted();
})->middleware('auth');


Route::get('/api/category/scripts/{id}', function($id){
      
  return ScriptsJson::get($id);
});

//get the category name
Route::get('api/category/name/{id}', function($id){
     
  return WorkoutCategoryName::get($id);
})->middleware('auth');


// ===================================================free trail =-==============================================
Route::prefix('free')->group(function () {

  Route::get('/', 'WelcomeController@show')->middleware('auth');

  Route::get('/thankyou', 'ThankyouController@index')->middleware('auth');

  Route::get('/home', 'FreeTrial\HomeController@index')->middleware('auth');
  Route::resource('home', 'FreeTrial\HomeController')->middleware('auth');


  Route::get('training/{pid}/{questid}', 'FreeTrial\TrainingController@show')->middleware('auth');
  Route::get('training/{pid}/{questid}/start-quiz', 'FreeTrial\TrainingController@startQuiz')->middleware('auth');
  Route::put('training/{pid}/{questid}/evaluate', 'FreeTrial\UserTrainingHistoryController@store')->middleware('auth');

  Route::post('training/{pid}/{questid}/videoWatched', 'FreeTrial\UserTrainingHistoryController@hasWatched')->middleware('auth');

  Route::resource('training', 'FreeTrial\TrainingController')->middleware('auth');

  Route::get('presentation/{name}', 'FreeTrial\ToolController@presentation')->middleware('auth');
  Route::resource('tools', 'FreeTrial\ToolController')->middleware('auth');

  Route::get('branding/my_certificate/{id}', 'BrandingController@certificate')->middleware('auth');
  Route::get('branding/download/{pid}/{id}', 'BrandingController@resource')->middleware('auth');
  Route::resource('branding', 'FreeTrial\BrandingController')->middleware('auth');

  Route::get('workouts/{pid}/{categoryid}', 'FreeTrial\WorkoutController@scripts')->middleware('auth');
  Route::resource('workouts', 'FreeTrial\WorkoutController')->middleware('auth');

});